package com.boraji.tutorial.springboot.controller;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class MethodExecutionCalculationAspect {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(MethodExecutionCalculationAspect.class);

    @Around("@annotation(com.boraji.tutorial.springboot.controller.TimeTrackingAspect)")
    public void around(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        joinPoint.proceed();
        long timeTaken = System.currentTimeMillis() - startTime;
        LOG.warn("Time Taken by {} is {}", joinPoint, timeTaken);
    }
}
